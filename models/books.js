const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/books')
const books = mongoose.model('book', {
    title: {
        type: String,
        minlength: [3, 'judul terlalu pendek'],
        maxlength: [30, 'judul terlalu panjang'],
        required: [true, 'judul harus diisi'],
        trim: true
    },
    author: {
        type: Array,
        required: [true, 'author belum terisi']
    },
    published_date: {
        type: Date,
        default: Date.now
    },
    pages: {
        type: Number,
        max: 1000
    },
    language: {
        type: String,
        maxlength: [20, 'penulisan salah'],
        require: true,
        trim: true
    },
    publisher_id: {
        type: String,
        maxlength: [20, 'kelebihan karakter cuy!']
    }
})

module.exports = books