const express = require('express')
const port = 4000
const host = "https://localhost:"
const route = require('./routers/router')

let app = express()
app.use(express.json())
require('./routers/router')(app)

app.use(route)

app.listen(port, () => {
    console.log(`Server was running on port ${host}${port}`)
})