const books = require('../models/books')

module.exports = (app) => {
    app.post("/books", async (req, res) => {
            try {
                let book = new books(req.body)
                let result = await book.save()
                res.status(201).send(result)
            } catch (error) {
                res.status(500).send(error)
            }
        }
    )
    app.get('/books', async(req,res)=>{
        try {
            let result = await books.find().exec()
            res.status(201).send(result)
        } catch (error) {
            res.status(500).send(error)
        }
    })
    app.put('/books/:id', async (req, res) => {
        try {
            let book = await books.findById(req.params.id).exec()
            book.set(req.body)
            let result = await book.save()
            res.status(201).send(result)
        } catch (error) {
            res.status(500).send(error)
        }
    })
    app.delete('/books/:id', async (req, res) => {
        try {
            let result = books.deleteOne().exec()
            res.status(201).send(result)
        } catch (error) {
            res.status(500).send(error)
        }

    })
}
    
    